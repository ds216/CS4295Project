package com.cs4295.team.fragment;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs4295.team.R;
import com.cs4295.team.util.APICallBuilder;
import com.cs4295.team.util.Sharedinfo;
import com.cs4295.team.util.Userinfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListmemberFragment extends Fragment {
    private View rootView;
    private ListView listview;
    private BaseAdapter adapter;
    private int teamid;
    private List<Userinfo> members = new ArrayList<Userinfo>();
    private Boolean isAdmin=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_addmember, container, false);
        teamid = getArguments().getInt("Teamid");
        isAdmin = getArguments().getBoolean("isAdmin");
        Log.d("teamid",teamid+"");
        ListView listview = (ListView)rootView.findViewById(R.id.listMember);
        registerForContextMenu(listview);
        rootView.findViewById(R.id.addMemberButton).setVisibility(View.GONE);
        SearchView sv = (SearchView) rootView.findViewById(R.id.searchMember);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener( ) {
            @Override
            public boolean  onQueryTextChange( String newText ) {
                List<Userinfo> searchResults = new ArrayList<Userinfo>();
                for(Userinfo u :members ){
                    if(u.getUsername().toUpperCase().contains(newText.toUpperCase()) || u.getName().toUpperCase().contains(newText.toUpperCase()) ||
                            u.getTel().contains(newText) || u.getEmail().toUpperCase().contains(newText.toUpperCase()) && !u.getEmail().equals("null"))
                        searchResults.add(u);
                }
                ListView list = (ListView) getView().findViewById(R.id.listMember);
                adapter = new UserAdapter(getActivity(),searchResults);
                list.setAdapter(adapter);
                list.invalidateViews();
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String arg0) {
                return true;
            }
        });
        getUser();
        return rootView;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.listMember) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            // Message obj = (Message) lv.getItemAtPosition(acmi.position);
            Log.d("position", acmi.position + "");
            Userinfo obj = (Userinfo) lv.getItemAtPosition(acmi.position);
            if(!obj.getTel().equals(null)  && !obj.getTel().equals("null") && !obj.getTel().isEmpty()) {
                String str = getString(R.string.PhoneCall);
                str = str + " " + obj.getName();
                menu.add(acmi.position, 1, 1, str);
            }
            if(!obj.getEmail().equals(null) && !obj.getEmail().equals("null") && !obj.getEmail().isEmpty()){
                String str = getString(R.string.Email);
                str = str + " " + obj.getName();
                menu.add(acmi.position, 2,2, str);
            }
            if(isAdmin == true) {
                if (obj.getIsAdmin() == 1 && obj.getUid() != PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()).getInt("uid", 0)) {
                    String str = getString(R.string.removeteamAdmin);
                    menu.add(acmi.position, 3, 3, str);
                } else if (obj.getIsAdmin() == 0 && obj.getUid() != PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()).getInt("uid", 0)) {
                    String str = getString(R.string.setteamAdmin);
                    menu.add(acmi.position, 4, 4, str);
                }
            }
            //menu.add(acmi.position);
        }
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        if(menuItemIndex==1) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + members.get(item.getGroupId()).getTel()));
            startActivity(callIntent);
        }else if(menuItemIndex==2){
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            String email= members.get(item.getGroupId()).getEmail();
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
        }else if(menuItemIndex==3){//remove
            setAdmin(members.get(item.getGroupId()).getUid(),0);
        }else if(menuItemIndex==4) {//set
            setAdmin(members.get(item.getGroupId()).getUid(),1);
        }
        //String text = String.format("Selected %d for item", menuItemIndex);
        //Toast.makeText(getActivity(),text,Toast.LENGTH_LONG).show();
        return true;
    }

    public  void getUser(){
        new AsyncTask<Void,String,String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {
                    Sharedinfo share = Sharedinfo.getInstance();
                    int uid = share.getUser().getUid();
                    String serverURL = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()).getString("Server", "ds216.net");
                    APICallBuilder Apicall = new APICallBuilder("http://" + serverURL + "/team/");
                    Apicall.setGETpara("handler=member&action=search");
                    JSONObject obj = new JSONObject();
                    obj.put("teamid", teamid);
                    obj.put("uid",0);
                    Log.d("getMember", obj.toString());
                    Apicall.setPOSTpara(obj.toString());
                    String HTML = Apicall.getResponse();
                    return HTML;
                }catch(Exception ex){
                    ex.printStackTrace();
                    return "";
                }
            }
            @Override
            protected void onPostExecute(String msg) {
                Log.d("result",msg);
                List<Userinfo> searchResults = new ArrayList<Userinfo>();
                try {
                    JSONObject json = new JSONObject(msg);
                    JSONArray arr = json.getJSONArray("users");
                    for(int i=0;i<arr.length();i++){
                        JSONObject userinfo = (JSONObject)arr.get(i);
                        Userinfo usr = new Userinfo(userinfo.getInt("uid"),userinfo.getString("username"),userinfo.getString("name"),userinfo.getString("tel"),userinfo.getString("email"));
                        if(userinfo.getInt("isadmin")==1)
                            usr.setIsAdmin(1);
                        searchResults.add(usr);
                    }
                    ListView list = (ListView) rootView.findViewById(R.id.listMember);
                    adapter = new UserAdapter(getActivity(),searchResults);
                    members = searchResults;
                    list.setAdapter(adapter);
                    list.invalidateViews();
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
        }.execute(null, null, null);
    }

    public  void setAdmin(int uid, int var){
        new AsyncTask<Integer,Void,String>() {
            @Override
            protected String doInBackground(Integer... params) {
                try {
                    Sharedinfo share = Sharedinfo.getInstance();
                    int uid = share.getUser().getUid();
                    String serverURL = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()).getString("Server", "ds216.net");
                    APICallBuilder Apicall = new APICallBuilder("http://" + serverURL + "/team/");
                    Apicall.setGETpara("handler=member&action=setAdmin");
                    JSONObject obj = new JSONObject();
                    obj.put("teamid", teamid);
                    obj.put("uid",params[0]);
                    obj.put("var",params[1]);
                    Log.d("getMember", obj.toString());
                    Apicall.setPOSTpara(obj.toString());
                    String HTML = Apicall.getResponse();
                    return HTML;
                }catch(Exception ex){
                    ex.printStackTrace();
                    return "";
                }
            }
            @Override
            protected void onPostExecute(String msg) {
                try {
                    JSONObject json = new JSONObject(msg);
                    if(json.getString("result").equals("True")){
                        getUser();
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        }.execute(uid, var);
    }


    public class UserAdapter extends BaseAdapter {
        private List<Userinfo> searchArrayList;

        private LayoutInflater mInflater;

        public UserAdapter(Context context, List<Userinfo> results) {
            searchArrayList = results;
            mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return searchArrayList.size();
        }

        public Object getItem(int position) {
            return searchArrayList.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            final int  pos = position;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.memberlist_item, null);
                CheckBox cb = (CheckBox)convertView.findViewById(R.id.selected);
                cb.setVisibility(View.INVISIBLE);
                holder = new ViewHolder();
                holder.member_username = (TextView) convertView.findViewById(R.id.member_username);
                holder.member_name = (TextView) convertView.findViewById(R.id.member_name);
                holder.isAdmin = (TextView) convertView.findViewById(R.id.isAdmin);
                holder.selected = (CheckBox) convertView.findViewById(R.id.selected);
                if(searchArrayList.get(pos).getIsAdmin()==1)
                    holder.isAdmin.setVisibility(View.VISIBLE);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.member_username.setText(searchArrayList.get(position).getUsername());
            holder.member_name.setText(searchArrayList.get(position).getName());
            return convertView;
        }

        class ViewHolder {
            TextView member_name;
            TextView member_username;
            CheckBox selected;
            TextView isAdmin;
        }
    }
}
