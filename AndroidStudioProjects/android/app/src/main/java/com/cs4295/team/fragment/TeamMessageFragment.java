package com.cs4295.team.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs4295.team.R;
import com.cs4295.team.util.APICallBuilder;
import com.cs4295.team.util.Message;
import com.cs4295.team.util.Sharedinfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Marcus on 3/27/2015.
 */
public class TeamMessageFragment extends Fragment {

    private int teamID = -1; // the id in the arraylist of team, not same as the real id
    Sharedinfo shared = Sharedinfo.getInstance();
    private ArrayList<Message> msgs = new ArrayList<Message>();
    private final int rows = 20;
    int page = 0;
    static Handler msgHandler;
    MessageAdapter adapter;
    ListView list;
    SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SwipeRefreshLayout swipeLayout;
    private Boolean isAdmin = false;
    private Menu menu;
    private Boolean isRefresh = false;

    {
        msgHandler = new Handler() {
            @Override
            public void handleMessage(android.os.Message msg) {
                if(msg.what==1) {
                    Log.d("Msg1", (String) msg.obj);
                    JSONObject json;
                    try {
                        msgs.clear();
                        json = new JSONObject((String) msg.obj);
                        JSONArray arr = json.getJSONArray("msg");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject item = arr.getJSONObject(i);
                            Message temp = new Message(item.getString("timestamp"), item.getInt("msgid"), item.getInt("replyid"), item.getInt("teamid"), item.getInt("uid"), item.getString("msg"), item.getString("title"),item.getString("name"),item.getString("tel"),item.getString("email"));
                            msgs.add(temp);
                        }
                        for(Message msgt : msgs)
                        Log.i("Msg",msgt.getTitle());
                        adapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                super.handleMessage(msg);
            }
        };
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        AsyncTask<Integer, Void, String> checkIsAdmin = new AsyncTask<Integer, Void, String>() {
            protected String doInBackground(Integer... paras) {
                String HTML = "";
                try {
                    String serverURL = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()).getString("Server", "ds216.net");
                    APICallBuilder Apicall = new APICallBuilder("http://" + serverURL + "/team/");
                    Apicall.setGETpara("handler=member&action=isAdmin");
                    JSONObject obj = new JSONObject();
                    obj.put("teamid", paras[0]);
                    obj.put("uid", paras[1]);
                    Log.d("DEBUG", obj.toString());
                    Apicall.setPOSTpara(obj.toString());
                    HTML = Apicall.getResponse();
                    Log.d("checkisAdmin", HTML);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return HTML;
            }
        };
        try {
            String str = checkIsAdmin.execute(teamID,PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()).getInt("uid",0)).get();

            JSONObject json = new JSONObject(str);
            isAdmin = json.getString("result").equals("true");
        } catch (Exception e) {
            e.printStackTrace();
        }
        inflater.inflate(R.menu.message, menu);
        if(isAdmin==false){
            menu.findItem(R.id.addmember).setVisible(false);
            menu.findItem(R.id.delmember).setVisible(false);
        }
       // super.onCreateOptionsMenu(menu,inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.new_message) {
            FragmentManager fragmentManager2 = getFragmentManager();
            FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
            NewMessageFragment fragment2 = new NewMessageFragment();
           // int team_id = Sharedinfo.getInstance().getTeams().get(relativeTeamID).getTeamid();
            fragment2.setTeamId(teamID);
            fragmentTransaction2.addToBackStack(null);
            fragmentTransaction2.replace(R.id.container, fragment2);
            fragmentTransaction2.commit();
            return true;
        }else if(id == R.id.addmember){
            FragmentManager fragmentManager2 = getFragmentManager();
            FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
            Fragment fragment2 = new AddmemberFragment();
            // int team_id = Sharedinfo.getInstance().getTeams().get(relativeTeamID).getTeamid();
            Bundle bundle = new Bundle();
            bundle.putInt("Teamid", teamID);
            fragmentTransaction2.addToBackStack(null);
            fragment2.setArguments(bundle);
            fragmentTransaction2.replace(R.id.container, fragment2);
            fragmentTransaction2.commit();
            return true;
        }else if(id == R.id.listmember){
            FragmentManager fragmentManager2 = getFragmentManager();
            FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
            Fragment fragment2 = new ListmemberFragment();
            // int team_id = Sharedinfo.getInstance().getTeams().get(relativeTeamID).getTeamid();
            Bundle bundle = new Bundle();
            bundle.putInt("Teamid", teamID);
            bundle.putBoolean("isAdmin", isAdmin);
            fragmentTransaction2.addToBackStack(null);
            fragment2.setArguments(bundle);
            fragmentTransaction2.replace(R.id.container, fragment2);
            fragmentTransaction2.commit();
            return true;
        }else if(id == R.id.delmember){
            FragmentManager fragmentManager2 = getFragmentManager();
            FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
            Fragment fragment2 = new DeletememberFragment();
            // int team_id = Sharedinfo.getInstance().getTeams().get(relativeTeamID).getTeamid();
            Bundle bundle = new Bundle();
            bundle.putInt("Teamid", teamID);
            fragmentTransaction2.addToBackStack(null);
            fragment2.setArguments(bundle);
            fragmentTransaction2.replace(R.id.container, fragment2);
            fragmentTransaction2.commit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        setHasOptionsMenu(true);
        if (teamID >= 0) {
            page = 0;
            //new checkIsAdmin().execute(teamID,PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()).getInt("uid",0));
            rootView = inflater.inflate(R.layout.fragment_messgelist, container, false);
            //TextView name = (TextView) rootView.findViewById(R.id.MsglistView);
            //name.setText("You are viewing Team " + shared.getTeams().get(relativeTeamID).getTeamname());
            GetMsg();
            Log.i("MsgList", msgs.toString());
            super.onActivityCreated(savedInstanceState);
            // Context mContext = getView().getContext();

            adapter = new MessageAdapter(getActivity(), msgs);
            list = (ListView) rootView.findViewById(R.id.MsglistView);
            swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
            list.setAdapter(adapter);
            registerForContextMenu(list);

            list.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    boolean enable = false;
                    if(list != null && list.getChildCount() > 0){
                        // check if the first item of the list is visible
                        boolean firstItemVisible = list.getFirstVisiblePosition() == 0;
                        // check if the top of the first item is visible
                        boolean topOfFirstItemVisible = list.getChildAt(0).getTop() == 0;
                        // enabling or disabling the refresh layout
                        enable = firstItemVisible && topOfFirstItemVisible;
                    }
                    swipeLayout.setEnabled(enable);
                }
            });


            swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);
            swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

                @Override
                public void onRefresh() {
                    isRefresh = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            GetMsg();
                            swipeLayout.setRefreshing(false);
                            isRefresh=false;
                        }
                    }, 2000);
                }
            });

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    if(isRefresh==false) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("Msgid", msgs.get(position).getMsgid());
                        bundle.putInt("Teamid", teamID);
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        ViewMessageFragment fragment = new ViewMessageFragment();
                        fragment.setArguments(bundle);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.replace(R.id.container, fragment);
                        fragmentTransaction.commit();
                    }

                    //Toast.makeText(getActivity().getApplicationContext(),
                    //        "" + msgs.get(position).getMsgid() +","+msgs.get(position).getMsg(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        return rootView;
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.MsglistView) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
           // Message obj = (Message) lv.getItemAtPosition(acmi.position);
            Log.d("position", acmi.position + "");
            Message obj = (Message) lv.getItemAtPosition(acmi.position);
            if(!obj.getTelno().equals(null)  && !obj.getTelno().equals("null") && !obj.getTelno().isEmpty()) {
                String str = getString(R.string.PhoneCall);
                str = str + " " + obj.getName();
                menu.add(acmi.position, 1, 1, str);
            }
            if(!obj.getEmail().equals(null) && !obj.getEmail().equals("null") && !obj.getEmail().isEmpty()){
                String str = getString(R.string.Email);
                str = str + " " + obj.getName();
                menu.add(acmi.position, 2,2, str);
            }
            //menu.add(acmi.position);
        }
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        if(menuItemIndex==1) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + msgs.get(item.getGroupId()).getTelno()));
            startActivity(callIntent);
        }else if(menuItemIndex==2){
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            String email= msgs.get(item.getGroupId()).getEmail();
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
        }
        //String text = String.format("Selected %d for item", menuItemIndex);
        //Toast.makeText(getActivity(),text,Toast.LENGTH_LONG).show();
        return true;
    }

    public int getTeamID() {
        return teamID;
    }

    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }

    public void refresh(){
        GetMsg();
    }

    private Boolean GetMsg() {
        Log.d("DEBUG", "submiting");
        try {
            Thread thread=new Thread(
                    new Runnable(){
                        @Override
                        public void run(){
                            try{
                                String serverURL= PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("Server", "ds216.net");
                                APICallBuilder Apicall= new APICallBuilder("http://"+serverURL+"/team/");
                                Apicall.setGETpara("handler=message&action=get");
                                JSONObject obj = new JSONObject();
                                obj.put("teamid", teamID);
                                obj.put("start",page);
                                obj.put("rows",rows);
                                Log.d("GetMsg", obj.toString());
                                Apicall.setPOSTpara(obj.toString());
                                String HTML = Apicall.getResponse();
                                android.os.Message message = new android.os.Message();
                                message.what = 1;
                                message.obj = HTML;
                                msgHandler.sendMessage(message);
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
            );
            thread.start();
            return true;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }






    public class MessageAdapter extends BaseAdapter {
        private  ArrayList<Message> searchArrayList;

        private LayoutInflater mInflater;

        public MessageAdapter(Context context, ArrayList<Message> results) {
            searchArrayList = results;
            mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return searchArrayList.size();
        }

        public Object getItem(int position) {
            return searchArrayList.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.messagelist_item, null);
                holder = new ViewHolder();
                holder.topic = (TextView) convertView.findViewById(R.id.msg_topic);
                holder.brief = (TextView) convertView.findViewById(R.id.msg_brief);
                holder.writer = (TextView) convertView.findViewById(R.id.msg_writer);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.topic.setText(searchArrayList.get(position).getTitle());
            holder.brief.setText(searchArrayList.get(position).getMsg());
            holder.writer.setText(searchArrayList.get(position).getName() +" "+ DateUtils.getRelativeTimeSpanString(searchArrayList.get(position).getDate().getTime(), new java.util.Date().getTime(), DateUtils.MINUTE_IN_MILLIS)+"");
            Log.d("Title",searchArrayList.get(position).getTitle());
            Log.d("Msg",searchArrayList.get(position).getMsg());
            return convertView;
        }

        class ViewHolder {
            TextView topic;
            TextView brief;
            TextView writer;
        }
    }
}
