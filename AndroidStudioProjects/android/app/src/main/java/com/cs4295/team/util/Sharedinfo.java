package com.cs4295.team.util;

import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;

public class Sharedinfo {
	private static Sharedinfo instance = new Sharedinfo();
	private ArrayList<Teaminfo> teams = new ArrayList<Teaminfo>();
	private Userinfo user = new Userinfo(0,null,null,null,null);

	public ArrayList<Teaminfo> getTeams() {
		return teams;
	}

	public void setTeams(ArrayList<Teaminfo> teams) {
		Log.d("SHARE","SET TEAMS");
		this.teams = teams;
	}

    public Teaminfo getTeam(int teamid){
        for(Teaminfo team : teams){
            if(team.getTeamid()==teamid){
                return team;
            }
        }
        return null;
    }
	public void clear(){instance = new Sharedinfo();}

	public static Sharedinfo getInstance() {
		return instance;
	}

	public static void setInstance(Sharedinfo instance) {
		Sharedinfo.instance = instance;
	}

	public Userinfo getUser() {
		return user;
	}

	public void setUser(Userinfo user) {
		this.user = user;
	}

	public void removeTeam(int teamid){
		Iterator<Teaminfo> it = teams.iterator();
		while(it.hasNext()){
			Teaminfo ti = it.next();
			if(ti.getTeamid()==teamid){
				it.remove();
				Log.d("removeTeam",teamid +"removed");
			}
		}
	}
}
