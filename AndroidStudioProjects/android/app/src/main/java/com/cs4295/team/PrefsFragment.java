package com.cs4295.team;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs4295.team.util.APICallBuilder;
import com.cs4295.team.util.MD5;

import org.json.JSONObject;

public class PrefsFragment extends PreferenceFragment {
    private SharedPreferences sharedPreferences;
    Preference loginInfo;
    String olddpn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                // Load the preferences from an XML resource
                addPreferencesFromResource(R.xml.preferences);
        loginInfo =  findPreference("usr_loginas");
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        olddpn = sharedPreferences.getString("displayname", "");
        loginInfo.setTitle(getString(R.string.pref_loginas) + " " + sharedPreferences.getString("username", "") +" " + "(" + sharedPreferences.getString("pref_displayname", "") + ")");
        Preference pref_displayname = findPreference("pref_displayname");
        pref_displayname.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                new changeDisplayName().execute(
                        sharedPreferences.getInt("uid",0)+"",
                        sharedPreferences.getString("token", ""),
                        (String) newValue
                );
                return true;
            }
        });
        Preference notify_receive = findPreference("notify_receive");
        notify_receive.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                int val;
                if((Boolean) newValue == true){
                    val = 1;
                }else{
                    val = 0;
                }
                new changeReceive().execute(
                        sharedPreferences.getInt("uid",0)+"",
                        sharedPreferences.getString("token", ""),
                        (val+"")
                );
                return true;
            }
        });
        Preference pref_userPassword = findPreference("pref_userPassword");
        pref_userPassword.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View layout = inflater.inflate(R.layout.customsetting, (ViewGroup) getView(),false);
                final EditText opwd= (EditText) layout.findViewById(R.id.setting_et_old_pass);
                final EditText npwd= (EditText) layout.findViewById(R.id.setting_et_new_pass);
                final EditText repwd= (EditText) layout.findViewById(R.id.setting_et_rep_new_pass);
                final TextView tv = (TextView) layout.findViewById(R.id.Messgae_text);
                builder.setView(layout);
                builder.setTitle(getString(R.string.pref_userPassword));
                DialogInterface.OnClickListener SubmitClick = new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("submitted","1");
                        if(npwd.getText().toString().equals(repwd.getText().toString())){
                            MD5 md5encode = new MD5();
                            new changePassword().execute(
                                    sharedPreferences.getInt("uid",0)+"",
                                    sharedPreferences.getString("token", ""),
                                    md5encode.getMD5EncryptedString(opwd.getText().toString()),
                                    md5encode.getMD5EncryptedString(npwd.getText().toString())
                            );
                            dialog.dismiss();
                        }else{
                            Toast.makeText(getActivity(), getString(R.string.pwdnotmatch),Toast.LENGTH_LONG).show();
                        }
                    }
                };
                builder.setPositiveButton(getString(R.string.btn_submit), SubmitClick);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return true;
            }
        });
    }


    private class changeReceive extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... paras) {
            String HTML="";
            try {
                String serverURL = sharedPreferences.getString("Server", "ds216.net");
                APICallBuilder Apicall = new APICallBuilder("http://" + serverURL + "/team/");
                Apicall.setGETpara("handler=user&action=chrec");
                JSONObject obj = new JSONObject();
                obj.put("uid", paras[0]);
                obj.put("token", paras[1]);
                obj.put("val", paras[2]);
                Log.d("DEBUG", obj.toString());
                Apicall.setPOSTpara(obj.toString());
                HTML = Apicall.getResponse();
                Log.d("cpwd",HTML);
            }catch(Exception e){
                e.printStackTrace();
            }
            return HTML;
        }
    }

    private class changePassword extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... paras) {
            String HTML="";
            try {
                String serverURL = sharedPreferences.getString("Server", "ds216.net");
                APICallBuilder Apicall = new APICallBuilder("http://" + serverURL + "/team/");
                Apicall.setGETpara("handler=user&action=chpwd");
                JSONObject obj = new JSONObject();
                obj.put("uid", paras[0]);
                obj.put("token", paras[1]);
                obj.put("opwd", paras[2]);
                obj.put("npwd", paras[3]);
                Log.d("DEBUG", obj.toString());
                Apicall.setPOSTpara(obj.toString());
                HTML = Apicall.getResponse();
                Log.d("cpwd",HTML);
            }catch(Exception e){
                e.printStackTrace();
            }
            return HTML;
        }

        protected void onPostExecute(String result) {
            try {
                JSONObject json = new JSONObject(result);
                if(json.getString("result").equals("True"))
                    Toast.makeText(getActivity(), getString(R.string.updateSuccess), Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(getActivity(), getString(R.string.updateFail), Toast.LENGTH_LONG).show();
            }catch(Exception e){
                e.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.updateError), Toast.LENGTH_LONG).show();
            }
        }
    }

    private class changeDisplayName extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... paras) {
            String HTML="";
            try {
                String serverURL = sharedPreferences.getString("Server", "ds216.net");
                APICallBuilder Apicall = new APICallBuilder("http://" + serverURL + "/team/");
                Apicall.setGETpara("handler=user&action=chdpn");
                JSONObject obj = new JSONObject();
                obj.put("uid", paras[0]);
                obj.put("token", paras[1]);
                obj.put("ndpn", paras[2]);
                Log.d("DEBUG", obj.toString());
                Apicall.setPOSTpara(obj.toString());
                HTML = Apicall.getResponse();
                Log.d("res",HTML);
            }catch(Exception e){
                e.printStackTrace();
            }
            return HTML;
        }

        protected void onPostExecute(String result) {
            try {
                JSONObject json = new JSONObject(result);
                if(json.getString("result").equals("True")) {
                    Toast.makeText(getActivity(), getString(R.string.updateSuccess), Toast.LENGTH_LONG).show();
                    loginInfo.setTitle(getString(R.string.pref_loginas) + " " + sharedPreferences.getString("username", "") + "(" + sharedPreferences.getString("pref_displayname", "") + ")");
                }else {
                    Toast.makeText(getActivity(), getString(R.string.updateFail), Toast.LENGTH_LONG).show();
                    loginInfo.setTitle(getString(R.string.pref_loginas) + " " + sharedPreferences.getString("username", "") + "(" + olddpn + ")");
                }
            }catch(Exception e){
                e.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.updateError), Toast.LENGTH_LONG).show();
            }
        }
    }

}