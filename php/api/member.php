<?
class memberHandler{
	
	function main($para){
		switch($para['action']){
			case 'add':
				return $this->addMember($para['teamid'], $para['uid']);
			break;

			case 'get':
				return $this->getMembers($para['teamid'],$para['uid']);
			break;
			
			case 'search':
				return $this->searchMembers($para['teamid'],$para['uid'],$para['hint']);

			case 'del':
				return $this->delMember($para['teamid'], $para['uid']);
			break;
			
			case 'isAdmin':
				return $this->isAdmin($para['teamid'], $para['uid']);
				break;
				
			case 'setAdmin':
				return $this->setAdmin($para['teamid'], $para['uid'], $para['var']);
				break; 
		}
	}
	
	function addMember($teamid, $uid){
		$value = array();
		for($i=0;$i<count($uid);$i++){
			$value[] = '(\''.$uid[$i].'\',\''.$teamid.'\',0)';
		}
		$addsql = implode(",",$value);
		$arr=array();
		$sql = 'INSERT into team_user (`uid`, `teamid`,`isadmin`) VALUES '.$addsql;
		echo $sql;
		$query = $GLOBALS['mysqli']->query($sql);
		if(!$query){
			printf("Error: %s\n", $GLOBALS['mysqli']->error);
			$arr['result'] = 'false';
		}else{
			$arr['result'] = 'true';
			$handler = new GCMHandler();
			$handler -> sendAddtoTeamMsg($teamid,$uid);
		}
		return $arr;
	}

	function getMembers($teamid,$uid){
		$arr=array();
		$sql = 'SELECT u.uid,u.username,u.name,u.tel,u.email,m.isadmin FROM user u, team_user m WHERE m.uid=u.uid AND m.teamid = \''.$teamid.'\' AND m.uid != \''.$uid.'\' Order by m.isadmin DESC, u.uid';
		$query = $GLOBALS['mysqli']->query($sql);
		if(!$query){
			printf("Error: %s\n", $GLOBALS['mysqli']->error);
			$arr['result'] = 'false';
		}else{
			$members = array();
			while($result = $query->fetch_array(MYSQLI_ASSOC)){
				$members[]=$result;
			}
			$arr['users']=$members;
			$arr['result'] = 'true';
		}
		return $arr;
	}
	
	function searchMembers($teamid,$uid,$hint){
		$arr=array();
		$sql = 'SELECT u.uid,u.username,u.name,u.tel,u.email,m.isadmin FROM user u, team_user m WHERE 
			m.uid=u.uid AND 
				(u.username LIKE \'%'.$hint.'%\' OR 
				u.name LIKE \'%'.$hint.'%\' OR 
				u.tel LIKE \'%'.$hint.'%\' OR 
				u.email LIKE \'%'.$hint.'%\'
				) AND  
			m.teamid = \''.$teamid.'\' AND 
			m.uid != \''.$uid.'\' 
			Order by m.isadmin DESC, u.uid';
		$query = $GLOBALS['mysqli']->query($sql);
		if(!$query){
			printf("Error: %s\n", $GLOBALS['mysqli']->error);
			$arr['result'] = 'false';
		}else{
			$members = array();
			while($result = $query->fetch_array(MYSQLI_ASSOC)){
				$members[]=$result;
			}
			$arr['users']=$members;
			$arr['result'] = 'true';
		}
		return $arr;
	}		

	function delMember($teamid, $uid){		
		$value = array();
		for($i=0;$i<count($uid);$i++){
			$value[] = $uid[$i];
		}
		$addsql = implode(",",$value);
		$arr=array();
		$sql = 'DELETE from team_user where `teamid` = \''.$teamid.'\' and `uid` IN ('.$addsql.')';
		$query = $GLOBALS['mysqli']->query($sql);
		if(!$query){
			printf("Error: %s\n", $GLOBALS['mysqli']->error);
			$arr['result'] = 'false';
		}else{
			$arr['result'] = 'true';
			$handler = new GCMHandler();
			$handler -> sendRemoveFromTeamMsg($teamid,$uid);
		}
		return $arr;
	}
	
	function isAdmin($teamid, $uid){
		$arr=array();
		$sql = 'SELECT `isadmin` FROM `team_user` WHERE `uid`=\''.$uid.'\' AND `teamid` = \''.$teamid.'\'';
		$query = $GLOBALS['mysqli']->query($sql);
		$result = $query->fetch_array(MYSQLI_ASSOC);
		if($result['isadmin']==1)$arr['result']="true";
		else $arr['result']="false";
		return $arr;
	}
	
	function setAdmin($teamid,$uid,$var){
		$arr = array();
		$sql = 'UPDATE `team_user` SET isadmin =\''.$var.'\' WHERE `uid`=\''.$uid.'\' AND `teamid` = \''.$teamid.'\'';	
		$query = $GLOBALS['mysqli']->query($sql);
		if($GLOBALS['mysqli']->affected_rows == 1){	
			$arr['result'] = 'True';
		}else{
			$arr['result'] = 'False';
		}
		return $arr;
	}

}
?>