<?
class userHandler{
	
	function main($para){
		switch($para['action']){
			case 'login':
				return $this->login($para['username'],$para['password']);
			break;
			case 'check':
				return $this->checktoken($para['username'],$para['token']);
			break;
			case 'gcm':
				return $this->gcm($para['username'],$para['gcmid']);
			break;
			case 'checkToken':
				return $this->checktoken($para['token'],$para['username']);
			break;
			case 'searchUser':
				return $this->searchUser($para['teamid'],$para['str']);
			case 'logout':
				return $this->logout($para['username']);
			break;
			case 'chpwd':
				return $this->changePassword($para['uid'],$para['token'],$para['opwd'],$para['npwd']);
			break;
			case 'chdpn':
				return $this->changeDisplayName($para['uid'],$para['token'],$para['ndpn']);
			break;
			case 'chrec':
				return $this->changeReceive($para['uid'],$para['token'],$para['val']);
			break;
		}
	}

	function login($username,$password){
		$arr=array();
		$query = $GLOBALS['mysqli']->query('SELECT COUNT(*) FROM user WHERE username = \''.$username.'\' AND password = \''.$password.'\'');
		if(!$query){
			printf("Error: %s\n", $GLOBALS['mysqli']->error);
		}
		$result = $query->fetch_array(MYSQLI_BOTH);
		if($result[0]>0){
			$arr['result']='True';			
			$date = date('Y-m-d h:i:s', time()+3600);
			$query2 = $GLOBALS['mysqli']->query('UPDATE user SET token =\''.$this->createToken().'\', expireday=\''.$date.'\' WHERE  username = \''.$username.'\' AND password = \''.$password.'\'');
			$query = $GLOBALS['mysqli']->query('SELECT uid,username,token,name,tel,email FROM user WHERE username = \''.$username.'\' AND password = \''.$password.'\'');
			$results = $query->fetch_array(MYSQLI_ASSOC);
			foreach($results as $key=>$var){
				$arr[$key]=$var;	
			}
		}else{
			
			$arr['result']='False';
		}
		return $arr;
	}
	
	function checktoken($token,$username){// $token will pass to the server with MD5
		$arr=array();
		$date = date('Y-m-d h:i:s', time());
		$query = $GLOBALS['mysqli']->query('SELECT token FROM user WHERE username = \''.$username.'\' ');
		if(!$query){
			printf("Error: %s\n", $GLOBALS['mysqli']->error);
		}
		$result = $query->fetch_array(MYSQLI_BOTH);
		if(md5($result[0]) == $token){
			$query = $GLOBALS['mysqli']->query('SELECT uid,username,name,tel,email FROM user WHERE username = \''.$username.'\'');
			$results = $query->fetch_array(MYSQLI_ASSOC);
			foreach($results as $key=>$var){
				$arr[$key]=$var;	
			}
			$arr['result']='True';
		}else{
			$arr['result']='False';
		}
		return $arr;			
	}
	
	function gcm($username,$gcmid){
		$arr = array();
		$query = $GLOBALS['mysqli']->query('UPDATE user SET gcmId =\''.$gcmid.'\' WHERE  username = \''.$username.'\'');
		$arr['result']='True';
		if(!$query){
			$arr['result']='False';
		}
		return $arr;
	}
	
	function searchUser($teamid,$str){
		$arr=array();
		$start = 0;
		$rows = 50;
		$sql = 'SELECT `uid`,`username`,`name`,`tel`,`email` from `user` where `uid` NOT IN (SELECT `uid` FROM `team_user` WHERE `teamid` = \''.$teamid.'\') AND `username` LIKE \'%'.$str.'%\' Order by `uid` DESC LIMIT '.$start.', '.$rows;
		$query = $GLOBALS['mysqli']->query($sql);
		if(!$query){
			printf("Error: %s\n", $GLOBALS['mysqli']->error);
		}else{
			$msg = array();
			while($result = $query->fetch_array(MYSQLI_ASSOC)){
				$msg[]=$result;
			}
			$arr['users']=$msg;
		}
		return $arr;
	}
	
	function createToken() {
   		$PassStr = Array(0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z);
    	for($i=0;$i<32;$i++) {
			mt_srand((double)microtime()*1000000);
			$randy = mt_rand(0,60);
			$pass .=$PassStr[$randy];
		}
		return $pass;
	}
	
	function logout($username){
		$sql = 'UPDATE `user` SET token=\'\',gcmId=\'\', expireday=\'\'  where username = \''.$username.'\'';
		$query = $GLOBALS['mysqli']->query($sql);
		$arr = array();
		$arr['result']='True';
		return $arr;
	}
	
	function changePassword($uid,$token,$oldPassword,$newPassword){
		$arr =array();
		$sql = 'SELECT count(`uid`) FROM `user` WHERE `uid`=\''.$uid.'\' AND `token`=\''.$token.'\' AND password=\''.$oldPassword.'\'';
		$query = $GLOBALS['mysqli'] -> query($sql);
		$result = $query->fetch_array(MYSQLI_BOTH);
		if($result[0]>0){
			$nsql = 'UPDATE `user` SET password = \''.$newPassword.'\' WHERE `uid` = \''.$uid.'\'';
			$nquery = $GLOBALS['mysqli'] -> query($nsql);
			$arr['result'] = 'True';
		}else{
			$arr['result'] = 'False';
		}
		return $arr;
	}
	
	function changeDisplayName($uid,$token,$newDisplayName){
		$arr = array();
		$sql = 'UPDATE `user` SET name=\''.$newDisplayName.'\'  where uid = \''.$uid.'\' AND token = \''.$token.'\'';
		$query = $GLOBALS['mysqli']->query($sql);
		if($GLOBALS['mysqli']->affected_rows == 1){		
			$arr['result']='True';
		}else{
			$arr['result'] = 'False';
		}
		return $arr;
	}
	
	function changeReceive($uid,$token,$newval){
		$arr = array();
		$sql = 'UPDATE `user` SET receive=\''.$newval.'\'  where uid = \''.$uid.'\' AND token = \''.$token.'\'';
		$query = $GLOBALS['mysqli']->query($sql);
		if($GLOBALS['mysqli']->affected_rows == 1){		
			$arr['result']='True';
		}else{
			$arr['result'] = 'False';
		}
		return $arr;
	}
}
?>