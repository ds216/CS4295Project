-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Apr 12, 2015 at 04:30 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `cs4295`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `message`
-- 

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `msgid` int(11) NOT NULL auto_increment,
  `replyid` int(11) NOT NULL,
  `teamid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `msg` text collate utf8_unicode_ci NOT NULL,
  `title` varchar(25) collate utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`msgid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `message`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `team`
-- 

DROP TABLE IF EXISTS `team`;
CREATE TABLE IF NOT EXISTS `team` (
  `teamid` int(11) NOT NULL auto_increment,
  `teamname` varchar(50) collate utf8_unicode_ci NOT NULL,
  `desrc` varchar(50) collate utf8_unicode_ci NOT NULL,
  `create` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`teamid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

-- 
-- Dumping data for table `team`
-- 

INSERT INTO `team` VALUES (1, 'Default Team', 'Default', '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `team_user`
-- 

DROP TABLE IF EXISTS `team_user`;
CREATE TABLE IF NOT EXISTS `team_user` (
  `uid` int(11) NOT NULL,
  `teamid` int(11) NOT NULL,
  `isadmin` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`uid`,`teamid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table `team_user`
-- 

INSERT INTO `team_user` VALUES (1, 1, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `uid` int(11) NOT NULL auto_increment,
  `username` varchar(50) collate utf8_unicode_ci NOT NULL,
  `password` varchar(50) collate utf8_unicode_ci NOT NULL,
  `name` varchar(25) collate utf8_unicode_ci NOT NULL,
  `tel` varchar(10) collate utf8_unicode_ci NOT NULL,
  `token` varchar(50) collate utf8_unicode_ci NOT NULL,
  `expireday` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `gcmId` text collate utf8_unicode_ci NOT NULL,
  `email` text collate utf8_unicode_ci,
  `receive` int(1) NOT NULL default '1',
  PRIMARY KEY  (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `user`
-- 

INSERT INTO `user` VALUES (1, 'test', '098f6bcd4621d373cade4e832627b4f6', 'Testing User', '12345678', '', '0000-00-00 00:00:00', '', NULL, 1);
